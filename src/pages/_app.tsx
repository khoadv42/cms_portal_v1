import '@/styles/variables.module.scss'
import '@/styles/globals.scss'
import type { AppProps } from 'next/app'

export default function App ({ Component, pageProps }: AppProps) {
  const Layout = (Component as any).Layout
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  )
}
