import { PageWrapper } from '@/components/PageWraper'
import { AdminLayout } from '@/components/layouts'

function Home () {
  return (
    <main className={''}>
      <h1>Index Page</h1>
    </main>
  )
}

export default PageWrapper(Home, AdminLayout)