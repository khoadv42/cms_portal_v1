import { ComponentType } from 'react'
import { NoneLayout } from './layouts'

export const PageWrapper = (Componenet: ComponentType, layout?: any) => {
  const Page = (props: any) => {
    return <Componenet {...props} />
  }

  Page.Layout = layout || NoneLayout

  console.log('layout :', layout);

  return Page
}
