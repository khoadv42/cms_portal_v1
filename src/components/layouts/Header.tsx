
import Image from 'next/image'
export default function HeaderComponent() {
    return <header>
        <div className='flex flex-start header'>
            <Image className='img-logo' src={'/assets/images/logo/Viettel_logo.png'} width={131} height={28} alt={''}></Image>
            <div className='header-title'>
                <div className='header-title-inner'>
                    CMS
                </div>
            </div>
        </div>
    </header>
}