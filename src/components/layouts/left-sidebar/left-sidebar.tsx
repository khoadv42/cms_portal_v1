import Image from 'next/image'
import { Inter } from 'next/font/google'
import { useState } from 'react'
import { AppPropsType } from 'next/dist/shared/lib/utils'
import { ComponentSvg, GlobalSvg, ServiceSvg, NewSvg, ImageSvg, FilterSvg, UserSvg } from './svg.component'

const inter = Inter({ subsets: ['latin'] })
interface SvpProps {
    isActive: boolean
}

interface ItemProps {
    name: string,
    href?: string,
    onClick?: () => void,
    isActive: boolean,
    children: React.FunctionComponent<SvpProps>,
}
export const Item = (props: ItemProps) => {
    const Children = props.children;
    return <li className={`left-sidebar-menu-body-list-item ${props.isActive ? 'selected' : ''}`} onClick={() => props.onClick?.()}>
        <div className='flex flex-start flex-row'>
            <div>
                {<Children isActive={props.isActive}/>}
            </div>
            <div>
                <a className=''>{props.name}</a>
            </div>
        </div>
    </li>
}

export default function LeftSidebar() {
    const [position, setPosition] = useState(0);
    const items = [
        {
            link: "",
            name: "Trang web",
            children: GlobalSvg
        },
        {
            link: "",
            name: "Component",
            children: ComponentSvg
        },
        {
            link: "",
            name: "Tin tức",
            children: NewSvg
        },
        {
            link: "",
            name: "Ảnh",
            children: ImageSvg
        },
        {
            link: "",
            name: "Filter",
            children: FilterSvg
        },
        {
            link: "",
            name: "User",
            children: UserSvg
        },
    ]
    const onSelected = (index: number) => {
        setPosition(index);
    }
    return (
        <section className='left-sidebar-menu'>
            <div className='left-sidebar-menu-header'>
                <p className='font-weight-bold'>Cấu hình</p>
            </div>
            <div className='left-sidebar-menu-body'>
                <ul className='left-sidebar-menu-body-list'>
                    {
                        items.map((item: any, index: number) => <Item onClick={() => {
                            onSelected(index)
                        } } key={index} name={item.name} isActive={index === position} children={item.children} />)
                    }
                </ul>
            </div>
        </section>
    )
}
