import { ReactNode, Suspense } from 'react'
import HeaderComponent from './Header'
import LeftSidebar from './left-sidebar/left-sidebar'

interface AdminLayoutProps {
  children: ReactNode
}

export default function AdminLayout (props: AdminLayoutProps) {
  const { children } = props

  return (
    <div>
      <Suspense fallback={<h1>Loading...</h1>}>
        <HeaderComponent></HeaderComponent>
        <div className='flex flex-row'>
          <LeftSidebar></LeftSidebar>
          <main className='admin-layout-main'>{children}</main>
        </div>
      </Suspense>
    </div>
  )
}
