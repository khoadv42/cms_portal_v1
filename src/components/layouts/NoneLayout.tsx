import { ReactNode } from 'react'

interface NoneLayoutProps {
  children: ReactNode
}

export default function NoneLayout (props: NoneLayoutProps) {
  const { children } = props
  return <>{children}</>
}
